from django import forms
from myapp.models import Reviews

class my_review(forms.ModelForm):
    class Meta:
        model = Reviews
        fields = '__all__'
        labels = {'text':'Give Your Opinion'}